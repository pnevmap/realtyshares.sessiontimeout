﻿using Orchard;
using Orchard.Mvc.Filters;
using Orchard.UI.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Orchard.ContentManagement;
using RealtyShares.SessionTimeout.Models;

namespace RealtyShares.SessionTimeout.Filters
{
    public class SessionWarningFilter : FilterProvider, IResultFilter
    {
        private readonly IResourceManager _resourceManager;
        private readonly IOrchardServices _services;

        public SessionWarningFilter(IResourceManager resourceManager, IOrchardServices services)
        {
            _resourceManager = resourceManager;
            _services = services;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            // Should only run on a full view rendering result
            if (!(filterContext.Result is ViewResult) || !filterContext.HttpContext.User.Identity.IsAuthenticated)
                return;            

            var settings = _services.WorkContext.CurrentSite.As<SessionTimeoutSettingsPart>();

            var url = new UrlHelper(filterContext.RequestContext);

            // Include the following script on all pages
            _resourceManager.RegisterFootScript(string.Format(@"
<script type=""text/javascript"">
setTimeout(function() {{
    alert('You will be automatically logged out in 1 minute due to inactivity.');
}}, {0});
setTimeout(function() {{
    location = '{2}'
}}, {1});
</script>
", 
            (settings.SessionDuration - 1) * 60000, 
            settings.SessionDuration * 60000 + 5000,
            url.Content("~/session-expired")));
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}