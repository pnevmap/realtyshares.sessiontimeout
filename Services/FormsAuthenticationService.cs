﻿using Orchard.ContentManagement;
using Orchard.Data;
using Orchard.Environment.Configuration;
using Orchard.Environment.Extensions;
using Orchard.Environment.Features;
using Orchard.Mvc;
using Orchard.Security;
using Orchard.Services;
using RealtyShares.SessionTimeout.Models;
using System;
using System.Linq;

namespace RealtyShares.SessionTimeout.Services
{
    [OrchardSuppressDependency("Orchard.Security.Providers.FormsAuthenticationService")]
    public class FormsAuthenticationService : IAuthenticationService
    {
        private readonly Orchard.Security.Providers.FormsAuthenticationService wrapped;

        public FormsAuthenticationService(
            ShellSettings settings,
            IClock clock,
            IContentManager contentManager,
            IHttpContextAccessor httpContextAccessor,
            IFeatureManager featureManager,
            IRepository<SessionTimeoutSettingsPartRecord> repository)
        {
            wrapped = new Orchard.Security.Providers.FormsAuthenticationService(settings, clock, contentManager, httpContextAccessor);

            // The try...catch block ensures we don't have yellow screens of death when
            // the module is being enabled.
            try
            {
                if (featureManager.GetEnabledFeatures().Any(f => f.Id == "RealtyShares.SessionTimeout"))
                {
                    var record = repository.Fetch(r => true).FirstOrDefault();
                    wrapped.ExpirationTimeSpan = TimeSpan.FromMinutes(record == null ? SessionTimeoutSettingsPartRecord.DefaultDuration : record.SessionDuration);
                }
            }
            catch (Exception) { }
        }

        public void SignIn(IUser user, bool createPersistentCookie)
        {
            wrapped.SignIn(user, createPersistentCookie);
        }

        public void SignOut()
        {
            wrapped.SignOut();
        }

        public void SetAuthenticatedUserForRequest(IUser user)
        {
            wrapped.SetAuthenticatedUserForRequest(user);
        }

        public IUser GetAuthenticatedUser()
        {
            return wrapped.GetAuthenticatedUser();
        }
    }
}