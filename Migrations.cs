﻿using Orchard.ContentManagement;
using Orchard.Data.Migration;
using Orchard.Environment.Extensions;
using RealtyShares.SessionTimeout.Models;

namespace RealtyShares.SessionTimeout
{
    public class Migrations : DataMigrationImpl
    {
        private readonly IContentManager _contentManager;

        public Migrations(IContentManager contentManager)
        {
            _contentManager = contentManager;
        }

        public int Create()
        {
            SchemaBuilder.CreateTable(typeof(SessionTimeoutSettingsPartRecord).Name,
                table => table
                    .ContentPartRecord()
                    .Column<int>("SessionDuration", c => c.WithDefault(SessionTimeoutSettingsPartRecord.DefaultDuration))
                );

            return 1;
        }
    }
}