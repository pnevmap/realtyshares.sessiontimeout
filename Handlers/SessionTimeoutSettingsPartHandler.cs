using JetBrains.Annotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Data;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using RealtyShares.SessionTimeout.Models;

namespace RealtyShares.SessionTimeout.Handlers
{
    public class SessionTimeoutSettingsPartHandler : ContentHandler
    {
        public Localizer T { get; set; }

        public SessionTimeoutSettingsPartHandler()
        {
            T = NullLocalizer.Instance;
        }

        public SessionTimeoutSettingsPartHandler(IRepository<SessionTimeoutSettingsPartRecord> repository)
        {
            Filters.Add(StorageFilter.For(repository));
            Filters.Add(new ActivatingFilter<SessionTimeoutSettingsPart>("Site"));
        }

        protected override void GetItemMetadata(GetContentItemMetadataContext context)
        {
            if (context.ContentItem.ContentType != "Site")
                return;
            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("Users")));
        }
    }
}
